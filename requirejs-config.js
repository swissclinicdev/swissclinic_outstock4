var config = {
    config: {
        mixins: {
            'mage/collapsible': {
                'js/collapsible-mixin': true
            }
        }
    },
    map: {
        '*': {
            'amasty_appendaround': 'Amasty_Blog/js/vendor/appendaround/appendaround'
        }
    }
};
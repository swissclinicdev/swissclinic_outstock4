define([
    'jquery',
], function ($) {

    var hideProps = {},
        showProps = {};

    hideProps.height =  'hide';
    showProps.height =  'show';

    return function (widget) {

        $.widget('mage.collapsible', widget, {

            _scrollToTopIfVisible: function (elem) {
                if (window.outerWidth < 768) {
                    $(document).scrollTop(elem.offsetTop)
                }
            },
        });

        return $.mage.collapsible;
    };
});